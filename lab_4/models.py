from django.db import models
from datetime import datetime
from django.utils import timezone
from time import gmtime, strftime
import pytz

class Message(models.Model):
    def convert_time():
        return timezone.now() + timezone.timedelta(hours=7)

    curr_time = datetime.time(datetime.now())
    name = models.CharField(max_length=27)
    email = models.EmailField()
    message = models.TextField()
    created_date = models.DateTimeField(default=convert_time)

    def __str__(self):
        return self.message
