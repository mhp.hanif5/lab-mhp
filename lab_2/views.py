from django.shortcuts import render
from lab_1.views import mhs_name, birth_date


#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = "Don\'t get too close, it\'s dark inside. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent feugiat augue in euismod lacinia. Aliquam erat volutpat. Nunc eu risus laoreet, tincidunt mauris sed, mollis magna. Phasellus eget malesuada justo. Vivamus interdum orci in lectus tristique dignissim. Phasellus sit amet ex semper, convallis diam vitae, vulputate dolor. Aliquam in sagittis odio."

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)
